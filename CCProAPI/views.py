import json
import os

import requests
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

currencies = requests.get('https://api.coinmarketcap.com/v1/ticker/')
# currencies = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/coinmarketcap.json')).read()
rates = requests.get('http://apilayer.net/api/live?access_key=cd1747e1d9ea0ba51a5771aea42cc7d4&currencies=EUR,CZK&source=USD&format=1')
# rates = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/apilayer.json')).read()

@api_view(['POST'])
def evaluate_portfolio(request):
    response = {}

    portfolio = parse_input_data(request.data)
    sum_usd = calculate_usd(portfolio)  # sum all coins in USD
    sum_original = calculate_original(portfolio)  # sum original prices of coins in USD
    sum = sum_usd
    if 'convert' in request.data:
        if sum > -1:
            sum = convert(sum_usd, request.data['convert'])
            if sum_original > -1:
                sum_original = convert(sum_original, request.data['convert'])
                response = createResponseOriginal(sum, request.data['convert'], sum_original)
            else:
                response = createResponse(sum, request.data['convert'])
            return JsonResponse(response)
        else:
            return HttpResponse(status=400)         # invalid data
    else:
        if sum != -1:
            if sum_original > -1:
                response = createResponseOriginal(sum, "USD", sum_original)
            else:
                response = createResponse(sum, "USD")
            return JsonResponse(response)
        else:
            return HttpResponse(status=400)         # invalid data


def get_users_portfolio(params):
    users_portfolio = []
    keys = int(len(params) / 3)
    for key in range(1, keys, 1):
        coin = {
            'no' : int(key) - 1,
            'name' : params['currency_'+str(key)],
            'amount': params['amount_'+str(key)],
            'price': params['price_'+str(key)]
        }
        users_portfolio.append(coin)
    return users_portfolio


def parse_input_data(data):
    portfolio = data['portfolio']
    return portfolio


def calculate_usd(portfolio):
    global currencies
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    sum = 0
    for key in portfolio:
        for coin in currenciesJson:
            if coin['name'] == key['name']:
                sum += float(key['amount']) * float(coin['price_usd'])
    return sum


def calculate_original(portfolio):
    sum = 0
    for key in portfolio:
        if 'price' in key:
            sum += float(key['amount']) * float(key['price'])
        else:
            return -1
    return sum


def convert(sum_usd, currency):
    global rates
    ratesJson = json.loads(rates.text)
    # ratesJson = json.loads(rates)
    target = "USD" + currency.upper()
    if target in ratesJson['quotes']:
        return sum_usd * float(ratesJson['quotes'][target])
    return sum_usd


def createResponseOriginal(sum, conversion, original):
    return {"value" : {"total" : sum, "currency" : conversion}, "original" : {"total" : original, "currency" : conversion}}


def createResponse(sum, conversion):
    return {"value" : {"total" : sum, "currency" : conversion}}