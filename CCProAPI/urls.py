from django.urls import path

from . import views

app_name = 'CCProAPI'

# "name":"bitcoin",
# "amount":"1.0125",
# "price":"1200",
# "convert":"EUR"

urlpatterns = [
    path('api', views.evaluate_portfolio, name='api'),
    # url(r'^api/$', views.evaluate_portfolio, name='snippet-list'),
    # url(r'^api/(?P<pk>[0-9]+)/highlight/$', snippet_highlight, name='snippet-highlight'),
    # path('^(?P<pk>[0-9]+)/$', views., name='index'),
    # url(r'^snippets/(?P<pk>[0-9]+)/$', views.snippet_detail),

]
