from django.urls import path

from . import views

app_name = 'CCProApp'

urlpatterns = [
    path('', views.select_currencies, name='index'),
    path('forms', views.select_currencies, name='selection'),
    path('ajax/select_currency', views.select_currencies_item, name='select_item'),
    path('portfolio', views.portfolio, name='portfolio'),
    path('selection_submit', views.selection_submit, name='selection_submit'),
]
