import ast

import praw as praw
from django.http import HttpResponse, QueryDict
from django.template import Context, loader
import json, requests

from django.utils.datastructures import MultiValueDict
from matplotlib.dates import DateFormatter
from pylab import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import os


currencies = requests.get('https://api.coinmarketcap.com/v1/ticker/')
# currencies = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/coinmarketcap.json')).read()
rates = requests.get('http://apilayer.net/api/live?access_key=cd1747e1d9ea0ba51a5771aea42cc7d4&currencies=EUR,CZK&source=USD&format=1')
# rates = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/apilayer.json')).read()
#reddits = requests.get('https://www.reddit.com/r/CryptoCurrency/hot.json?limit=10')
#reddits = requests.get('https://www.reddit.com/r/CryptoCurrency/search.json?q=bitcoin&limit=10')
# reddits = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/reddit.json')).read()

def portfolio(request):
    global currencies
    global rates
    template = loader.get_template('CCProPortfolio/portfolio.html')
    users_portfolio = get_users_portfolio(request.GET)
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    ratesJson = json.loads(rates.text)
    # ratesJson = json.loads(rates)
    reddits = get_reddits(users_portfolio)
    context = {
        'cryptos': currenciesJson,
        'portfolio': users_portfolio,
        'rates': ratesJson,
        'reddits': reddits,
    }
    return HttpResponse(template.render(context, request))


def get_reddits(users_portfolio):
    global currencies
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    reddit = praw.Reddit(client_id='SJurp0621yfjrg',
                         client_secret='Fh5AJ7ss3uwGgxm5F6hN1BPLn5A',
                         redirect_uri='http://localhost:8080',
                         user_agent='ccprofessional_bot 0.9')
    reddit_template = 'https://www.reddit.com/r/CryptoCurrency/search.json?limit=10&q='
    reddits = []
    for key in users_portfolio:
        print("URL: " + str(reddit_template + (key['name']).replace(" ", "+")))
        reddit_request = requests.get((reddit_template + (key['name']).replace(" ", "+")), headers = {'User-agent': 'ccprofessional_bot 0.9'})
        reddit_result = json.loads(reddit_request.text)
        reddits += reddit_result['data']['children']
    return reddits


def coin_graph(request):
    global currencies
    coin = get_currency(request.GET)
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)

    fig=Figure()
    ax=fig.add_subplot(111)

    coinInitial = float(coin['price']) * float(coin['amount'])

    coinNow = 0
    for curr in currenciesJson:
        if curr['name'] == coin['name']:
            coinNow = float(curr['price_usd']) * float(coin['amount'])

    x = ["Initial", "Now"]
    y = [coinInitial, coinNow]

    label = coin['name']

    ax.plot(x, y, label=label,color='C'+str(coin['no']))
    ax.legend(loc=2)

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

def portfolio_graph(request):
    global currencies

    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    params_dict = ast.literal_eval(request.GET['params'][12:-1])

    qdict = QueryDict('', mutable=True)
    qdict.update(MultiValueDict(params_dict))

    users_portfolio = get_users_portfolio(qdict)
    print("PORTFOLIO: " + str(users_portfolio))
    no_currencies = len(users_portfolio)

    fig=Figure()
    ax=fig.add_subplot(111)

    coinsInitial = [None] * no_currencies
    labels = [None] * no_currencies
    i = 0
    for p in users_portfolio:
        coinsInitial[i] = float(p['price']) * float(p['amount'])
        labels[i] = p['name']
        i += 1

    i = 0
    coinsNow = [None] * no_currencies
    for p in users_portfolio:
        for curr in currenciesJson:
            if curr['name'] == p['name']:
                coinsNow[i] = float(curr['price_usd']) * float(p['amount'])
                i += 1

    x = ["Initial", "Now"]

    values = []
    a_list = []
    for i in range(0, no_currencies):
        values.append((coinsInitial[i],coinsNow[i]))
    print(values)

    # y = np.stack(values)
    ax.stackplot(x, values, labels=labels)
    ax.legend(loc=2)

    canvas=FigureCanvas(fig)
    response=HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response


# get 'name', 'amount' and 'price' [USD] from the form's request parameters
def get_currency(params):
    currency = {
        'no': params['no'], 'name': params['currency'], 'amount': params['amount'], 'price': params['price']
    }
    return currency


def get_users_portfolio(params):
    users_portfolio = []
    keys = int(len(params) / 3)
    for key in range(1, keys, 1):
        coin = {
            'no' : int(key) - 1,
            'name' : params['currency_'+str(key)],
            'amount': params['amount_'+str(key)],
            'price': params['price_'+str(key)]
        }
        users_portfolio.append(coin)
    return users_portfolio
