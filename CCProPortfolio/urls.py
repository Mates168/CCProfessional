from django.urls import path

from . import views

app_name = 'CCProPortfolio'

urlpatterns = [
    path('index', views.portfolio, name='index'),
    path('portfolio_graph', views.portfolio_graph, name='portfolio_graph'),
    path('coin_graph', views.coin_graph, name='coin_graph'),
    path('reddits', views.get_reddits, name='reddits'),
]
